// registrr app
appdk.newApp({
    render: ({ bridge, intent, props }) => {
        return document.createElement({
            // element name
            tag: 'div',

            // style
            style: {
                display: 'flex',
                flexDirection: 'column',
                height: '100%',
                width: '100%',
                background: '#1def33',
                alignItems: 'center',
                justifyContent: 'space-evenly'
            },

            // element content
            children: ["PUT YOUR JSON IN THIS ARRAY AND REBUILD"]
        });
    },
});
